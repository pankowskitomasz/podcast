import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelsS2Component } from './channels-s2.component';

describe('ChannelsS2Component', () => {
  let component: ChannelsS2Component;
  let fixture: ComponentFixture<ChannelsS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelsS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelsS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
