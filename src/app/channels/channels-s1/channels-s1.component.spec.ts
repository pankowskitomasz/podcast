import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelsS1Component } from './channels-s1.component';

describe('ChannelsS1Component', () => {
  let component: ChannelsS1Component;
  let fixture: ComponentFixture<ChannelsS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelsS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelsS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
