import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChannelsRoutingModule } from './channels-routing.module';
import { ChannelsComponent } from './channels/channels.component';
import { ChannelsS1Component } from './channels-s1/channels-s1.component';
import { ChannelsS2Component } from './channels-s2/channels-s2.component';


@NgModule({
  declarations: [
    ChannelsComponent,
    ChannelsS1Component,
    ChannelsS2Component
  ],
  imports: [
    CommonModule,
    ChannelsRoutingModule
  ]
})
export class ChannelsModule { }
